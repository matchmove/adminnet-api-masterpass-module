<?php

namespace App\Modules\MasterPass\Models\User\Checkout;

use App\Modules\Core\Models\BaseModel;
use Illuminate\Support\Arr;

/**
 * @SWG\Definition(
 *     definition="MasterpassCheckout",
 *     @SWG\Property(property="id", type="integer", default="1"),
 *     @SWG\Property(property="mp_init_id", type="string", default="26"),
 *     @SWG\Property(property="user_hash_id", type="string", default="BE4FFCEE9840B5FEAE1CEFACDE283EDF61EE7A213A344EB459464BD0F5A51653"),
 *     @SWG\Property(property="first_name", type="string", default="John"),
 *     @SWG\Property(property="last_name", type="string", default="Doe"),
 *     @SWG\Property(property="card_number", type="string", default="XXXXXXXXXXXX12345"),
 *     @SWG\Property(property="merchant_checkout_id", type="string", description="", default="BE4FFCEE9840B5FEAE1CEFACDE283EDF61EE7A213A344EB459464BD0F5A51653"),
 *     @SWG\Property(property="merchant_name", type="string",  default="Merchant's Guild"),
 *     @SWG\Property(property="billing_address", type="string", default="Kenyon Rd"),
 *     @SWG\Property(property="shipping_address", type="string", default="North Hampshire"),
 *     @SWG\Property(property="checkout_auth_request", type="string",  default="{'details':'goes here'}"),
 *     @SWG\Property(property="checkout_auth_response", type="string",  default="{'details':'goes here'}"),
 *     @SWG\Property(property="status", type="string", default="failed"),
 *     @SWG\Property(property="date_created", type="string", format="date",  default="2016-09-28 09:50:02"),
 *     @SWG\Property(property="date_modified", type="string", format="date", default="2016-09-28 09:50:02"),
 * )
 */
class Transaction extends BaseModel
{
    // The DB connection
    protected $connection = 'IH_MP';

    // The DB table
    protected $table = 'user_checkout_transactions';

    // Hidden fields
    protected $hidden = [ 'mp_init_id', 'id' ];

    const NORMAL_FILTERS = [
        "shipping_address"  =>  ['shipping_address', 'like', '%%%s%%'],
        "billing_address"   =>  ['billing_address', 'like', '%%%s%%'],
        "merchant_name"     =>  ['merchant_name', 'like', '%%%s%%'],
        "with_pairing"      =>  ['with_pairing', '=', '%s'],
        "user_hash_id"      =>  ['user_hash_id', '=', '%s'],
        "date_created"      =>  ['date_created', '=', '%s'],
        "card_number"       =>  ['card_number', '=', '%s'],
        "first_name"        =>  ['first_name', 'like', '%%%s%%'],
        "last_name"         =>  ['last_name', 'like', '%%%s%%'],
    ];

    const DATE_FILTER = [
        'from_date' => [ 'date_created', '>=', null ],
        'to_date' => [ 'date_created', '<=', null ]
    ];


    /**
     * Function to retrieve masterpass checkouts record
     *
     * @param $params
     * @return mixed
     */
    public function retrieve($params)
    {
        $result = $this->useValues($this, $params)
            ->useFilters(self::NORMAL_FILTERS)
            ->useDateFilters(self::DATE_FILTER)
            ->useSortFilters([ [ $params['sort_by'], null, null ] ])
            ->apply()
            ->paginate($params['records_per_page']);

        return $result;
    }
}